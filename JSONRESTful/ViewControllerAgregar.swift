//
//  ViewControllerAgregar.swift
//  JSONRESTful
//
//  Created by carlos pumayalla on 11/23/21.
//  Copyright © 2021 empresa. All rights reserved.
//

import UIKit

class ViewControllerAgregar: UIViewController {

    @IBOutlet weak var guardar: UIButton!
    @IBOutlet weak var actualizar: UIButton!
    @IBOutlet weak var nombreTxt: UITextField!
    @IBOutlet weak var generoTxt: UITextField!
    @IBOutlet weak var duracionTxt: UITextField!
    var pelicula:Peliculas?
    override func viewDidLoad() {
        super.viewDidLoad()
        if pelicula == nil {
            guardar.isEnabled = true
            actualizar.isEnabled = false
        }else{
            guardar.isEnabled = false
            actualizar.isEnabled = true
            nombreTxt.text = pelicula?.nombre
            generoTxt.text = pelicula?.genero
            duracionTxt.text = pelicula?.duracion
        }
    }
    
    @IBAction func guardarBtn(_ sender: Any) {
        let nombre = nombreTxt.text!
        let genero = generoTxt.text!
        let duracion = duracionTxt.text!
        let datos = ["usuarioId": 1, "nombre": "\(nombre)", "genero": "\(genero)", "duracion": "\(duracion)"] as Dictionary<String, Any>
        let ruta = "http://localhost:3000/peliculas"
        metodoPOST(ruta: ruta, datos: datos)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actualizarBtn(_ sender: Any) {
        let nombre = nombreTxt.text!
        let genero = generoTxt.text!
        let duracion = duracionTxt.text!
        let datos = ["usuarioId": 1, "nombre": "\(nombre)", "genero": "\(genero)", "duracion": "\(duracion)"] as Dictionary<String, Any>
        let ruta = "http://localhost:3000/peliculas/\(pelicula!.id)"
        metodoPUT(ruta: ruta, datos: datos)
        navigationController?.popViewController(animated: true)
    }
    
    func metodoPOST (ruta:String, datos:[String:Any]) {
        let url:URL = URL(string: ruta)!
        var request = URLRequest(url: url)
        let session = URLSession.shared
        
        request.httpMethod = "POST"
        
        let params = datos
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions.prettyPrinted)
        }catch{
            
        }
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            if (data != nil){
                do{
                    let dict = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves)
                    print(dict)
                }catch{
                    
                }
            }
        })
        task.resume()
    }
    
    func metodoPUT (ruta:String, datos:[String:Any]) {
        let url:URL = URL(string: ruta)!
        var request = URLRequest(url: url)
        let session = URLSession.shared
        
        request.httpMethod = "PUT"
        
        let params = datos
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions.prettyPrinted)
        }catch{
            
        }
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            if (data != nil){
                do{
                    let dict = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves)
                    print(dict)
                }catch{
                    
                }
            }
        })
        task.resume()
    }
}
